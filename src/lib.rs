use std::time::Instant;

pub mod y2015;
pub mod y2016;
pub mod y2017;
pub mod y2018;
pub mod y2019;
pub mod y2020;
pub mod y2021;
pub mod y2022;

pub const INPUT_DATA_DIR: &'static str = "input_data";
pub const TEST_DATA_DIR: &'static str = "input_data/test_data";

type Res<T> = Result<T, String>;

#[derive(Debug)]
pub struct Solution<T, V> {
    a: T,
    b: V,
    a_millis: u128,
    b_millis: u128,
}

pub trait Input {
    fn from_file(file: impl AsRef<str>) -> Self;

    fn from_sample(file: impl AsRef<str>) -> Self;
}

pub trait Problem<T, V> {
    fn part_one(&mut self) -> Res<T>;

    fn part_two(&mut self) -> Res<V>;

    fn solve(&mut self) -> Res<Solution<T, V>> {
        let a_t = Instant::now();
        let a = self.part_one()?;
        let a_millis = a_t.elapsed().as_millis();

        let b_t = Instant::now();
        let b = self.part_two()?;
        let b_millis = b_t.elapsed().as_millis();
        Ok(Solution {
            a,
            b,
            a_millis,
            b_millis,
        })
    }
}
