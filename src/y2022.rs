use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};

pub struct Day1 {
    data: Option<String>,
}

impl Input for Day1 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day1 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut max = 0;

            let mut count = 0;
            for line in data.lines() {
                match line.parse::<u64>() {
                    Ok(calorie_count) => {
                        count += calorie_count;
                    }
                    Err(_) => {
                        if count > max {
                            max = count;
                        }
                        count = 0;
                    }
                }
            }

            Ok(max)
        } else {
            Err("[2022 (Day1)]: No data found".into())
        }
    }

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut elves = Vec::new();

            let mut count = 0;
            for line in data.lines() {
                match line.parse::<u64>() {
                    Ok(calorie_count) => {
                        count += calorie_count;
                    }
                    Err(_) => {
                        elves.push(count);
                        count = 0;
                    }
                }
            }
            elves.push(count);

            elves.sort();
            elves.reverse();
            Ok(elves[0] + elves[1] + elves[2])
        } else {
            Err("[2022 (Day1)]: No data found".into())
        }
    }
}

pub struct Day2 {
    data: Option<String>,
}

impl Input for Day2 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day2 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut total = 0;
            for line in data.lines() {
                let round = line.split(" ").collect::<Vec<_>>();

                let opponent = round[0];
                let me = round[1];

                match opponent {
                    // Rock v Rock
                    "A" if me == "X" => {
                        total += 1 + 3;
                    }
                    // Rock v Paper
                    "A" if me == "Y" => {
                        total += 2 + 6;
                    }
                    // Rock v scissor
                    "A" if me == "Z" => {
                        total += 3;
                    }
                    // Paper v Rock
                    "B" if me == "X" => {
                        total += 1 + 0;
                    }
                    // Paper v Paper
                    "B" if me == "Y" => {
                        total += 2 + 3;
                    }
                    // Paper v scissor
                    "B" if me == "Z" => {
                        total += 3 + 6;
                    }
                    // Scissor v Rock
                    "C" if me == "X" => {
                        total += 1 + 6;
                    }
                    // Scissor v Paper
                    "C" if me == "Y" => {
                        total += 2 + 0;
                    }
                    // Scissor v scissor
                    "C" if me == "Z" => {
                        total += 3 + 3;
                    }
                    _ => {}
                }
            }
            Ok(total)
        } else {
            Err(String::from("[2022 Day2]: No data found"))
        }
    }

    // Rock: 1
    // Paper: 2
    // Scissors: 3

    // Win: 6
    // Tie: 3
    // Lose 0

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut total = 0;
            for line in data.lines() {
                let round = line.split(" ").collect::<Vec<_>>();

                let opponent = round[0];
                let me = round[1];

                match opponent {
                    // Rock Lose -> Scissor
                    "A" if me == "X" => {
                        total += 3;
                    }
                    // Rock Draw -> Rock
                    "A" if me == "Y" => {
                        total += 1 + 3;
                    }
                    // Rock Win -> Paper
                    "A" if me == "Z" => {
                        total += 2 + 6;
                    }
                    // Paper Lose -> Rock
                    "B" if me == "X" => {
                        total += 1;
                    }
                    // Paper Draw -> Paper
                    "B" if me == "Y" => {
                        total += 2 + 3;
                    }
                    // Paper Win -> Scissor
                    "B" if me == "Z" => {
                        total += 3 + 6;
                    }
                    // Scissor Lose -> Paper
                    "C" if me == "X" => {
                        total += 2;
                    }
                    // Scissor Draw -> Scissor
                    "C" if me == "Y" => {
                        total += 3 + 3;
                    }
                    // Scissor Win -> Rock
                    "C" if me == "Z" => {
                        total += 1 + 6;
                    }
                    _ => {}
                }
            }
            Ok(total)
        } else {
            Err(String::from("[2022 Day2]: No data found"))
        }
    }
}

pub struct Day3 {
    data: Option<String>,
}

impl Input for Day3 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day3 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total = data
                .lines()
                .map(|line| line.split_at(line.len() / 2))
                .flat_map(|(left, right)| left.chars().find(|c| right.contains(*c)))
                .fold(0, |acc, c| {
                    if c.is_ascii_lowercase() {
                        acc + (c as u32) - 96
                    } else {
                        acc + (c as u32) - 38
                    }
                });

            Ok(total as u64)
        } else {
            Err(String::from("[2022 Day3]: No data found"))
        }
    }

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total = data
                .lines()
                .step_by(3)
                .zip(
                    data.lines()
                        .skip(1)
                        .step_by(3)
                        .zip(data.lines().skip(2).step_by(3)),
                )
                .flat_map(|(first, (second, third))| {
                    // TODO: this could possibly be optimized
                    // by sorting the strings and iterating
                    // over the smallest first.
                    first
                        .chars()
                        .find(|c| second.contains(*c) && third.contains(*c))
                })
                .fold(0, |acc, c| {
                    if c.is_ascii_lowercase() {
                        acc + (c as u32) - 96
                    } else {
                        acc + (c as u32) - 38
                    }
                });
            Ok(total as u64)
        } else {
            Err(String::from("[2022 Day3]: No data found"))
        }
    }
}

pub struct Day4 {
    data: Option<String>,
}

impl Input for Day4 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day4 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total =
                data.lines()
                    .map(|l| l.split(",").collect::<Vec<_>>())
                    .fold(0, |acc, splits| {
                        let l = splits[0]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();
                        let r = splits[1]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();

                        if (l[0] <= r[0] && l[1] >= r[1]) || (r[0] <= l[0] && r[1] >= l[1]) {
                            acc + 1
                        } else {
                            acc
                        }
                    });
            Ok(total)
        } else {
            Err(String::from("[2022 Day4]: No data found"))
        }
    }

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total =
                data.lines()
                    .map(|l| l.split(",").collect::<Vec<_>>())
                    .fold(0, |acc, splits| {
                        let l = splits[0]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();
                        let r = splits[1]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();

                        let mut a = [&l, &r];
                        a.sort();
                        let [l, r] = a;

                        if l[1] >= r[0] {
                            acc + 1
                        } else {
                            acc
                        }
                    });
            Ok(total)
        } else {
            Err(String::from("[2022 Day4]: No data found"))
        }
    }
}
